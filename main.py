from abc import ABC, abstractmethod
import datetime


class LibraryManagementSystem:
    def __init__(self, userType=None, username=None, password=None):
        self.userType = userType
        self.username = username
        self.password = password

    def login(self):
        # prompt the user to enter their username and password
        username = input("Enter your username: ")
        password = input("Enter your password: ")

        # verify that the credentials match a user account in the system

        self.username = username
        self.password = password
        print("Login successful!")


    def register(self):
        # prompt the user to enter their personal and contact information
        self.userType = input("you are (student, admin ): ")
        self.username = input("Enter your name: ")
        self.password = input("Enter your email address: ")




    def logout(self):
        self.userType = None
        self.username = None
        self.password = None
        print("Logout successful!")


class User(ABC):
    def __init__(self, id=None, name=None,books=None):
        self.id = id
        self.name = name
        self.books = books

    @abstractmethod
    def verifyAccount(self):
        # Implement verification logic here
        print("Account verified.")

    @abstractmethod
    def checkAccount(self):
        pass

    @abstractmethod
    def getBookInfo(self):
        # Implement book info retrieval logic here
        if len(self.books) == 0:
            print("No books associated with this account.")
        else:
            print("Books associated with this account:")
            for book in self.books:
                print(book.title)



class Account:
    def __init__(self, borrowedBooks=None, reservedBooks=None, returnedBooks=None, lostBooks=None, fineAmount=None):
        self.borrowedBooks = borrowedBooks or []
        self.reservedBooks = reservedBooks or []
        self.returnedBooks = returnedBooks or []
        self.lostBooks = lostBooks or []
        self.fineAmount = fineAmount or 0

    def calculateFine(self):
        # Implementation for calculating the fine amount for an account
        if self.fineAmount == 0:
            print("No fines owed.")
        else:
            print(f"Total fine amount: ${self.fineAmount}")

    def borrowBook(self, book):
        # Implementation for borrowing a book and updating the account's records
        self.borrowedBooks.append(book)
        print(f"Borrowed book: {book.title} by {book.author}")
        due_date = datetime.datetime.now() + datetime.timedelta(days=30)
        print(f"Due date: {due_date.strftime('%Y-%m-%d')}")

    def returnBook(self, book):
        # Implementation for returning a book and updating the account's records
        if book in self.borrowedBooks:
            self.borrowedBooks.remove(book)
            self.returnedBooks.append(book)
            print(f"Returned book: {book.title} by {book.author}")
        else:
            print(f"Book not found: {book.title} by {book.author}")

    def reserveBook(self, book):
        # Implementation for reserving a book and updating the account's records
        self.reservedBooks.append(book)
        print(f"Reserved book: {book.title} by {book.author}")
        print("Please pick up the book within 2 days.")

    def reportLostBook(self, book):
        # Implementation for reporting a lost book and updating the account's records
        if book in self.borrowedBooks:
            self.borrowedBooks.remove(book)
            self.lostBooks.append(book)
            self.fineAmount += 50
            print(f"Reported lost book: {book.title} by {book.author}")
            print("A fine of $50 has been added to your account.")
        else:
            print(f"Book not found: {book.title} by {book.author}")


class Book:
    def __init__(self, title=None, author=None, ISBN=None, publication=None):
        self.title = title
        self.author = author
        self.ISBN = ISBN
        self.publication = publication

    def showDate(self):
        # Implementation for showing the due date for a book
        print("Due date: 30 days from checkout.")

    def reservationStatus(self):
        # Implementation for checking the reservation status of a book
        print("Not reserved.")

    def feedback(self):
        # Implementation for providing feedback on a book
        print("Please provide feedback on this book:")
        # ...


class Librarian(User):
    def __init__(self, name=None, password=None, id=None):
        super().__init__(id=id, name=name)
        self.password = password

    def verifyAccount(self):
        # Implementation for verifying a librarian's account
        if self.id == "librarian" and self.password == "library123":
            print("Account verified.")
        else:
            print("Invalid credentials.")

    def checkAccount(self):
        # Implementation for checking the details of a librarian's account
        print(f"Name: {self.name}, ID: {self.id}")

    def getBookInfo(self):
        # Implementation for getting information about a book
        pass

    def addBook(self, book):
        # Implementation for adding a book to the library database
        print(f"Added book: {book.title} by {book.author} to the library database.")

    def deleteBook(self, book):
        # Implementation for deleting a book from the library database
        print(f"Deleted book: {book.title} by {book.author} from the library database.")

    def updateBook(self, book):
        # Implementation for updating the information of a book in the library database
        print(f"Updated book: {book.title} by {book.author} in the library database.")

    def search(self, query):
        # Implementation for searching the library database for books based on a query
        print(f"Searching for books containing '{query}'...")
        # ...


class LibraryDatabase:
    def __init__(self, books=None):
        self.books = books or []

    def addBook(self, book):
        # Implementation for adding a book to the library database
        self.books.append(book)
        print(f"Book added: {book.title} by {book.author}")

    def deleteBook(self, book):
        # Implementation for deleting a book from the library database
        if book in self.books:
            self.books.remove(book)
            print(f"Book deleted: {book.title} by {book.author}")
        else:
            print(f"Book not found: {book.title} by {book.author}")

    def updateBook(self, book):
        # Implementation for updating the information of a book in the library database
        if book in self.books:
            print(f"Book updated: {book.title} by {book.author}")
        else:
            print(f"Book not found: {book.title} by {book.author}")

    def displayBook(self, book):
        # Implementation for displaying the details of a book
        print(f"Title: {book.title}\nAuthor: {book.author}\nPublisher: {book.publisher}\nISBN: {book.isbn}")

    def search(self, query):
        # Implementation for searching the library database for books based on a query
        matches = []
        for book in self.books:
            if query.lower() in book.title.lower() or query.lower() in book.author.lower():
                matches.append(book)
        if len(matches) == 0:
            print("No books found.")
        else:
            print(f"{len(matches)} books found:")
            for book in matches:
                print(f"- {book.title} by {book.author}")



class Staff:
    def __init__(self, dept):
        self.dept = dept


class Student(User):
    def __init__(self, email=None, id=None, name=None):
        super().__init__(id=id, name=name)
        self.email = email

    def verifyAccount(self):
        # Implementation for verifying a student's account
        if self.id.startswith("S") and len(self.id) == 9:
            print("Account verified.")
        else:
            print("Invalid credentials.")

    def checkAccount(self):
        # Implementation for checking the details of a student's account
        print(f"Name: {self.name}, ID: {self.id}, Email: {self.email}")

    def getBookInfo(self):
        # Implementation for getting information about a book
        pass